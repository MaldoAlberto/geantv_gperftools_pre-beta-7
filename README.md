**GeantV performance results of pre-beta-7:**

FullCMS example:

-1000 events
-10-GeV electrons per event
-threads: 1
-10 tries per combination

-Each plot uses the average of the combinations configuration:

+ 0: scalar mode
+ 1: vector mode
+ 2: basketization enabled w/ scalar dispatch

-Scalar/vector config: Fi Pj Gk Mn:

+ F=magField
+ P=(EM)physics
+ G=geometry
+ M=msc

![](summary4.png)

![](summary7.png)

![](summary.png)