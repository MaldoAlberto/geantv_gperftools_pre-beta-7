Characteristics:

geant4 10.04.p3 version


Fedora WorkStation 29



gcc 8.2.1


veccore 0.5.2



Vc 1.3.3



AMD R A10-7700k radeon r7, 10 compute cores 4c+6g × 4

Instruction Set Extensions: AVX



Cache: 4MB

- Level 1 cache size 2 x 96 KB 3-way set associative shared instruction caches
- Level 1 cache size 4 x 16 KB 4-way set associative data caches
- Level 2 cache size 2 x 2 MB 16-way set associative shared caches



![](hcpuPerEvent.png)


![](hCpuSpeedup.png)