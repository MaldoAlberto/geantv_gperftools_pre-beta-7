#!/usr/bin/python

from ROOT import TGraph
#import sys
import csv
from datetime import date
import array as arr
from ROOT import TCanvas, gROOT, TGraph, TH1F, TFile, gStyle, TLegend

savefile = TFile("x.root","update")

n = 6
hCpuSpeedup1T = TH1F("hCpuSpeedup1T", "CPU Speedup (1000 events);config;speed-up", n, 0, n)
hCpuSpeedup4T = TH1F("hCpuSpeedup4T", "CPU Speedup;config;speed-up", n, 0, n)
hCpuSpeedup1AT = TH1F("hCpuSpeedup1AT", "CPU time per step;config;CPU time (usec)", n, 0, n)
hCpuSpeedup4AT = TH1F("hCpuSpeedup4AT", "CPU time per step;config;CPU time (usec)", n, 0, n)

class Job:
    """ Job information
    """
 
    def __init__(self,  iname, iNthread, iNprim, iNtrack, iNstep, iNsnext, iNphys, iNmagf, iNsmall, iNpush, iNkill, ibdr,
                 realtime, cputime):
        """ constructor creates the data series"""

        self.name = iname
        self.nthread = iNthread
        self.nprim = iNprim
        self.ntrack = iNtrack
        self.nstep = iNstep
        self.nsnext = iNsnext
        self.nphys = iNphys
        self.nmagf = iNmagf
        self.nsmall = iNsmall
        self.npush = iNpush
        self.nkill = iNkill
        self.bdrcross = ibdr
        self.cputime = cputime
        self.realtime = realtime


def fillWithPrefix(prefix, job, sum, yref,i) :
    """ Fills histograms with a prefixed label """
    hCpuSpeedup1T.Fill(" ", 0)
    hCpuSpeedup1AT.Fill(" ", 0)
    hCpuSpeedup4T.Fill(" ", 0) 
    hCpuSpeedup4AT.Fill(" ", 0) 

    hCpuSpeedup1T.Fill(" ", 0)
    hCpuSpeedup1AT.Fill(" ", 0)
    hCpuSpeedup4T.Fill(" ", 0)
    hCpuSpeedup4AT.Fill(" ", 0) 
 
    hCpuSpeedup1T.Fill(" ", 0)
    hCpuSpeedup1AT.Fill(" ", 0)
    hCpuSpeedup4T.Fill(" ", 0) 
    hCpuSpeedup4AT.Fill(" ", 0) 
   # if sum == 0: yref = job.cputime
    y = yref /job.cputime
    #y = job.cputime #nstep
    #y = job.cputime / job.nstep * 1.0e6

    #.. make sure all labels exist in all histograms
    if job.name[1]== "4":
	if job.nthread == 1:
		if prefix == "x: ":
		    hCpuSpeedup1T.Fill(" ", 0)
		    hCpuSpeedup1AT.Fill(" ", 0)
		    hCpuSpeedup4T.Fill(" ", 0) 
		    hCpuSpeedup4AT.Fill(" ", 0) 
            	    hCpuSpeedup1T.Fill(prefix + job.name, y)
    	            hCpuSpeedup1AT.Fill(prefix + job.name, 0)
            	    hCpuSpeedup4T.Fill(prefix + job.name, 0)
            	    hCpuSpeedup4AT.Fill(prefix + job.name, 0)
                else:
	    	    hCpuSpeedup1T.Fill(prefix + str(i), y)
	    	    hCpuSpeedup4T.Fill(prefix + str(i), 0)
	    	    hCpuSpeedup1AT.Fill(prefix + str(i), 0)
	    	    hCpuSpeedup4T.Fill(prefix + str(i), 0)
	elif job.nthread == 4:
		if prefix == "x: ":
		    hCpuSpeedup1T.Fill(" ", 0)
		    hCpuSpeedup1AT.Fill(" ", 0)
		    hCpuSpeedup4T.Fill(" ", 0) 
		    hCpuSpeedup4AT.Fill(" ", 0) 
            	    hCpuSpeedup1T.Fill(prefix + job.name, 0)
    	            hCpuSpeedup1AT.Fill(prefix + job.name, 0)
            	    hCpuSpeedup4T.Fill(prefix + job.name, y)
            	    hCpuSpeedup4AT.Fill(prefix + job.name, 0)
                else:
	    	    hCpuSpeedup1T.Fill(prefix + str(i), 0)
	    	    hCpuSpeedup4T.Fill(prefix + str(i), y)
	    	    hCpuSpeedup1AT.Fill(prefix + str(i), 0)
	    	    hCpuSpeedup4T.Fill(prefix + str(i), 0)

    elif job.name[14] == "0":

        if job.nthread == 1:

	    if prefix == "x: ":
            	hCpuSpeedup1T.Fill(prefix + str(i), y)
    	    	hCpuSpeedup1AT.Fill(prefix + str(i), 0)
            	hCpuSpeedup4T.Fill(prefix + str(i), 0)
            	hCpuSpeedup4AT.Fill(prefix + str(i), 0)
	    else:
	    	hCpuSpeedup1T.Fill(prefix + str(i), y)
	    	hCpuSpeedup4T.Fill(prefix + str(i), 0)
	    	hCpuSpeedup1AT.Fill(prefix + str(i), 0)
	    	hCpuSpeedup4T.Fill(prefix + str(i), 0)

        elif job.nthread == 4:
            if prefix == "x: ":
            	hCpuSpeedup1T.Fill(prefix + str(i), 0)
    	    	hCpuSpeedup1AT.Fill(prefix + str(i), 0)
            	hCpuSpeedup4T.Fill(prefix + str(i), y)
            	hCpuSpeedup4AT.Fill(prefix + str(i), 0)
	    else:
	    	hCpuSpeedup1T.Fill(prefix + str(i), 0)
	    	hCpuSpeedup4T.Fill(prefix + str(i), y)
	    	hCpuSpeedup1AT.Fill(prefix + str(i), 0)
	    	hCpuSpeedup4AT.Fill(prefix + str(i), 0)

    elif job.name[14] == "1":

        if job.nthread == 1:

	    if prefix == "x: ":
            	hCpuSpeedup1T.Fill(prefix + str(i)+"_ST1", 0)
    	    	hCpuSpeedup1AT.Fill(prefix + str(i)+"_ST1", y)
            	hCpuSpeedup4T.Fill(prefix + str(i)+"_ST1", 0)
            	hCpuSpeedup4AT.Fill(prefix + str(i)+"_ST1", 0)
	    else:
	    	hCpuSpeedup1T.Fill(prefix + str(i)+"_ST1", 0)
	    	hCpuSpeedup4T.Fill(prefix + str(i)+"_ST1", 0)
	    	hCpuSpeedup1AT.Fill(prefix + str(i)+"_ST1", y)
	    	hCpuSpeedup4AT.Fill(prefix + str(i)+"_ST1", 0)

        elif job.nthread == 4:
            if prefix == "x: ":
            	hCpuSpeedup1T.Fill(prefix + str(i), 0)
    	    	hCpuSpeedup1AT.Fill(prefix + str(i)+"_ST1", 0)
            	hCpuSpeedup4T.Fill(prefix + str(i), 0)
            	hCpuSpeedup4T.Fill(prefix + str(i), y)
	    else:
	    	hCpuSpeedup1T.Fill(prefix + str(i), 0)
	    	hCpuSpeedup4T.Fill(prefix + str(i), 0)
	    	hCpuSpeedup1AT.Fill(prefix + str(i), 0)
	    	hCpuSpeedup4AT.Fill(prefix + str(i), y)

    else:
        print "Job %s: %s  %d %d  %7.3f sec" % ( job.jobid, job.name, x, job.nthread, job.cputime )

   # return yref

def beautify(histo, icolor,pos,style) :
    ### setup space inside histograms
    histo.SetStats(0)
    histo.SetMinimum(0)
    histo.SetMaximum(1.5	)
    histo.LabelsOption("au","X")
    histo.SetLineColor(icolor)
    histo.SetLineStyle(1)
    histo.SetFillColor(icolor)
    histo.SetBarWidth(0.2);
    histo.SetLineWidth(2)
    histo.SetBarOffset(0.4*pos)
    histo.SetFillStyle(style)
    return

def readData(fileName):
    # Open file for reading
    file = open(fileName, "rt")

    reader = csv.reader(file)
    first=True

    jobs = []
    for row in reader :
        if not first :
            #print 'read: <%s>' % row

            name = row[0]
            nthread = int(row[1])
            nprim = int(row[2])
            ntrack = int(row[3])
            nstep = int(row[4])
            nsnext = int(row[5])
            nphys = int(row[6])
            nmagf = int(row[7])
            nsmall = int(row[8])
            npush = int(row[9])
            nkill = int(row[10])
            bdrcross = int(row[11])
            realtime = float(row[12])
            cputime = float(row[13])

            jobs.append( Job(name,nthread,nprim,ntrack,nstep,nsnext,nphys,nmagf,nsmall,npush,nkill,bdrcross,realtime,cputime) )
            #print len(jobs)

        else :
            #.. just skip reading header line in file
            first=False
            pass

    file.close()
    return jobs


### Entry point

if __name__ == "__main__":

    ### these are the full series (without normalization)
    jobs = readData("newdata.csv")
    print("jobs: %i" % len(jobs))

    refY = -1
    for job in jobs:
        n = job.name
	if job.name == "F0_P0_G0_M0_ST0":
		refY = job.cputime
		print refY

	if len(n)<3: fillWithPrefix("x: ", job, sum, refY,n[1])
	else:
		n = job.name
		f = int(n[1])
		p = int(n[4])
		g = int(n[7])
		m = int(n[10])

		sum = f + p + g + m
		if f == 0 and p == 0 and g == 0 and m == 0 : fillWithPrefix(" ", job, sum, refY,n[:11])
		#if f == 0 and p == 0 and m == 0 : refY = fillWithPrefix("G: ", job, sum, refY,g)
		#if f == 0 and g == 0 and m == 0 : refY = fillWithPrefix("P: ", job, sum, refY,p)
		#if p == 0 and g == 0 and m == 0 : refY = fillWithPrefix("F: ", job, sum, refY,f)
		if f + p + g + m == 3 or f + p + g + m == 8 : fillWithPrefix("x: ", job, sum, refY,n[:11])

    beautify(hCpuSpeedup1T, 2,1,3001)
    beautify(hCpuSpeedup4T, 4,5,3001)
    beautify(hCpuSpeedup1AT, 1,9,3008)
    beautify(hCpuSpeedup4AT, 3,13,3008)

    ### setup canvas
    c1 = TCanvas('c1','c1', 900, 600 )
    c1.SetGrid()
    #c1.Divide(1,4)

#    frame1 = c1.DrawFrame(3600,0,4000,2)
#    frame1 = c1.DrawFrame(0,0.,8000,15)
#    frame1 = c1.DrawFrame(0,0.4,1000,1.31)
#    frame1.GetXaxis().SetTitle("Date")
#    frame1.GetYaxis().SetTitle("Value")

#    graphs[i].SetMarkerStyle(20+i)
#    graphs[i].SetMarkerColor(i+1)
#    graphs[i].SetLineColor(i+1)
#    graphs[i].Draw("")

    #.. plot 1
    #c1.cd(1)
  #  print refY/(-4.002+refY)-1,refY/(-4.818+refY)-1,refY/(-6.528+refY)-1,refY/(-25.218+refY)-1
    a = arr.array('d', [0,0,refY/(-.002+refY)-1,0,refY/(-.818+refY)-1,refY/(-.528+refY)-1,refY/(-.218+refY)-1])
    b = arr.array('d', [0,0,0,refY/(-0.892+refY)-1,0,0,0])
    
    gStyle.SetErrorX(0.);
    gStyle.SetOptFit(1)
    #hCpuSpeedup1T.SetMarkerStyle(21);
    #hCpuSpeedup1T.SetMarkerColor(4);
    #hCpuSpeedup1T.SetLineColor(4);
    hCpuSpeedup1T.SetError(a)
    hCpuSpeedup1T.Draw('histbar')
    hCpuSpeedup1T.SetFillColorAlpha(20,0) 
    hCpuSpeedup1T.Draw("E same")

    #.. plot 2
    #c1.cd(2)
    #hCpuSpeedup4T.Draw('histbar same')

    #c1.cd(3)
    hCpuSpeedup1AT.SetBarOffset(0.4);
    
    #hCpuSpeedup1AT.SetMarkerStyle(21);
    #hCpuSpeedup1AT.SetMarkerColor(4);
    #hCpuSpeedup1AT.SetLineColor(4);
    hCpuSpeedup1AT.SetError(b)
    hCpuSpeedup1AT.SetFillColorAlpha(8,0) 
    hCpuSpeedup1AT.Draw('histbar same')
    hCpuSpeedup1AT.Draw('E same')

    #c1.cd(4)
    #hCpuSpeedup4AT.Draw('histbar same')

    legend = TLegend(0.1,0.7,0.48,0.9)
    legend.SetHeader("Process class","C") # option "C" allows to center the header
    legend.AddEntry(hCpuSpeedup1T,"1 thread","f")
    #legend.AddEntry(hCpuSpeedup4T,"4 thread","f")
    legend.AddEntry(hCpuSpeedup1AT,"1 thread and single track in 1","f")
    #legend.AddEntry(hCpuSpeedup4AT,"4 thread and single track in 1","f")
    legend.Draw()

    c1.SaveAs("hCpuSpeedup.png")

    savefile.Write()
    savefile.Close()
