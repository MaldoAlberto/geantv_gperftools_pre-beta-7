#!/usr/bin/python

from ROOT import TGraph
#import sys
import csv
from datetime import date
import array as arr
from ROOT import TCanvas, gROOT, TGraph, TH1F, TFile, gStyle, TLegend

savefile = TFile("x.root","update")

n = 6
hcpuPerEvent1T = TH1F("hcpuPerEvent1T", "CPU time per event (1000 events);config;CPU time (sec)", n, 0, n)
hcpuPerEvent4T = TH1F("hcpuPerEvent4T", "CPU time per event;config;CPU time (sec)", n, 0, n)
hcpuPerEvent1AT = TH1F("hcpuPerEvent1AT", "CPU time per event;config;CPU time (sec)", n, 0, n)
hcpuPerEvent4AT = TH1F("hcpuPerEvent4AT", "CPU time per event;config;CPU time (sec)", n, 0, n)

class Job:
    """ Job information
    """
 
    def __init__(self, iname, iNthread, iNprim, iNtrack, iNstep, iNsnext, iNphys, iNmagf, iNsmall, iNpush, iNkill, ibdr,
                 realtime, cputime):
        """ constructor creates the data series"""
        self.name = iname
        self.nthread = iNthread
        self.nprim = iNprim
        self.ntrack = iNtrack
        self.nstep = iNstep
        self.nsnext = iNsnext
        self.nphys = iNphys
        self.nmagf = iNmagf
        self.nsmall = iNsmall
        self.npush = iNpush
        self.nkill = iNkill
        self.bdrcross = ibdr
        self.cputime = cputime
        self.realtime = realtime


def fillWithPrefix(prefix, job,i,ref) :

    """ Fills histograms with a prefixed label """
    #y = job.cputime #nstep
    hcpuPerEvent1T.Fill(" ", 0)
    hcpuPerEvent1AT.Fill(" ", 0)
    hcpuPerEvent4T.Fill(" ", 0) 
    hcpuPerEvent4AT.Fill(" ", 0) 
    
    hcpuPerEvent1T.Fill(" ", 0)
    hcpuPerEvent1AT.Fill(" ", 0)
    hcpuPerEvent4T.Fill(" ", 0)
    hcpuPerEvent4AT.Fill(" ", 0) 
 
    hcpuPerEvent1T.Fill(" ", 0)
    hcpuPerEvent1AT.Fill(" ", 0)
    hcpuPerEvent4T.Fill(" ", 0) 
    hcpuPerEvent4AT.Fill(" ", 0) 


    y = (job.cputime)/ref

    #.. make sure all labels exist in all histograms
    
    if job.name[1]== "4":
	if job.nthread == 1:
		if prefix == "x: ":
		    hcpuPerEvent1T.Fill(" ", 0)
		    hcpuPerEvent1AT.Fill(" ", 0)
		    hcpuPerEvent4T.Fill(" ", 0) 
		    hcpuPerEvent4AT.Fill(" ", 0) 
            	    hcpuPerEvent1T.Fill(prefix + job.name, y)
    	            hcpuPerEvent1AT.Fill(prefix + job.name, 0)
            	    hcpuPerEvent4T.Fill(prefix + job.name, 0)
            	    hcpuPerEvent4AT.Fill(prefix + job.name, 0)
                else:
	    	    hcpuPerEvent1T.Fill(prefix + str(i), y)
	    	    hcpuPerEvent4T.Fill(prefix + str(i), 0)
	    	    hcpuPerEvent1AT.Fill(prefix + str(i), 0)
	    	    hcpuPerEvent4T.Fill(prefix + str(i), 0)


	if job.nthread == 4:
		if prefix == "x: ":
		    hcpuPerEvent1T.Fill(" ", 0)
		    hcpuPerEvent1AT.Fill(" ", 0)
		    hcpuPerEvent4T.Fill(" ", 0) 
		    hcpuPerEvent4AT.Fill(" ", 0) 
            	    hcpuPerEvent1T.Fill(prefix + job.name, 0)
    	            hcpuPerEvent1AT.Fill(prefix + job.name, 0)
            	    hcpuPerEvent4T.Fill(prefix + job.name, y)
            	    hcpuPerEvent4AT.Fill(prefix + job.name, 0)
                else:
	    	    hcpuPerEvent1T.Fill(prefix + str(i), 0)
	    	    hcpuPerEvent4T.Fill(prefix + str(i), y)
	    	    hcpuPerEvent1AT.Fill(prefix + str(i), 0)
	    	    hcpuPerEvent4T.Fill(prefix + str(i), 0)

    elif job.name[14] == "0":

        if job.nthread == 1:

            if prefix == "x: ":
		hcpuPerEvent1T.Fill(" ", 0)
		hcpuPerEvent1AT.Fill(" ", 0)
		hcpuPerEvent4T.Fill(" ", 0) 
		hcpuPerEvent4AT.Fill(" ", 0) 
            	hcpuPerEvent1T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], y)
    	        hcpuPerEvent1AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
            	hcpuPerEvent4T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
            	hcpuPerEvent4AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
            else:
	    	    hcpuPerEvent1T.Fill(prefix + str(i), y)
	    	    hcpuPerEvent4T.Fill(prefix + str(i), 0)
	    	    hcpuPerEvent1AT.Fill(prefix + str(i), 0)
	    	    hcpuPerEvent4T.Fill(prefix + str(i), 0)

        elif job.nthread == 4:
            if prefix == "x: ":
		hcpuPerEvent1T.Fill(" ", 0)
		hcpuPerEvent1AT.Fill(" ", 0)
		hcpuPerEvent4T.Fill(" ", 0) 
		hcpuPerEvent4AT.Fill(" ", 0) 
            	hcpuPerEvent1T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
    	    	hcpuPerEvent1AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
            	hcpuPerEvent4T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], y)
            	hcpuPerEvent4AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
	    else:
	    	hcpuPerEvent1T.Fill(prefix + str(i), 0)
	    	hcpuPerEvent4T.Fill(prefix + str(i), y)
	    	hcpuPerEvent1AT.Fill(prefix + str(i), 0)
	    	hcpuPerEvent4AT.Fill(prefix + str(i), 0)

    elif job.name[14] == "1":

        if job.nthread == 1:

	    if prefix == "x: ":
            	#hcpuPerEvent1T.Fill(" ", 0)
		#hcpuPerEvent1AT.Fill(" ", 0)
		#hcpuPerEvent4T.Fill(" ", 0) 
		#hcpuPerEvent4AT.Fill(" ", 0) 
		hcpuPerEvent1T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10]+"_ST1", 0)
    	    	hcpuPerEvent1AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10]+"_ST1", y)
            	hcpuPerEvent4T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10]+"_ST1", 0)
            	hcpuPerEvent4AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10]+"_ST1", 0)
	    else:
	    	hcpuPerEvent1T.Fill(prefix + str(i)+"_ST1", 0)
	    	hcpuPerEvent4T.Fill(prefix + str(i)+"_ST1", 0)
	    	hcpuPerEvent1AT.Fill(prefix + str(i)+"_ST1", y)
	    	hcpuPerEvent4AT.Fill(prefix + str(i)+"_ST1", 0)

        elif job.nthread == 4:
            if prefix == "x: ":
		#hcpuPerEvent1T.Fill(" ", 0)
		#hcpuPerEvent1AT.Fill(" ", 0)
		#hcpuPerEvent4T.Fill(" ", 0) 
		#hcpuPerEvent4AT.Fill(" ", 0) 
            	hcpuPerEvent1T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
    	    	hcpuPerEvent1AT.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
            	hcpuPerEvent4T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], 0)
            	hcpuPerEvent4T.Fill(prefix + job.name[1]+""+job.name[4]+""+job.name[7]+""+job.name[10], y)
	    else:
	    	hcpuPerEvent1T.Fill(prefix + str(i), 0)
	    	hcpuPerEvent4T.Fill(prefix + str(i), 0)
	    	hcpuPerEvent1AT.Fill(prefix + str(i), 0)
	    	hcpuPerEvent4AT.Fill(prefix + str(i), y)


    else:
        print "Job %s: %s  %d %d  %7.3f sec" % ( job.jobid, job.name, x, job.nthread, job.cputime )


    return

def beautify(histo, icolor, pos,style) :

    ### setup space inside histograms
    histo.SetStats(0)
    histo.SetMinimum(0)
    histo.SetMaximum(1.5)
    histo.LabelsOption("au","X")
    histo.SetLineColor(icolor)
    histo.SetLineStyle(1)
    histo.SetFillColor(2)
    histo.SetBarWidth(0.2);
    histo.SetLineWidth(2);
    histo.SetBarOffset(0.4*pos)
    histo.SetFillStyle(style)
    return

def readData(fileName):
    # Open file for reading
    file = open(fileName, "r")

    reader = csv.reader(file)
    first=True

    jobs = []
    for row in reader :
        if not first :
            #print 'read: <%s>' % row
            name = row[0]
            nthread = int(row[1])
            nprim = int(row[2])
            ntrack = int(row[3])
            nstep = int(row[4])
            nsnext = int(row[5])
            nphys = int(row[6])
            nmagf = int(row[7])
            nsmall = int(row[8])
            npush = int(row[9])
            nkill = int(row[10])
            bdrcross = int(row[11])
            realtime = float(row[12])
            cputime = float(row[13])

            jobs.append( Job(name,nthread,nprim,ntrack,nstep,nsnext,nphys,nmagf,nsmall,npush,nkill,bdrcross,realtime,cputime) )
            #print len(jobs)

        else :
            #.. just skip reading header line in file
            first=False
            pass

    file.close()
    return jobs


### Entry point
ref = 0
if __name__ == "__main__":

    ### these are the full series (without normalization)
    jobs = readData("newdata.csv")
    print("jobs: %i" % len(jobs))

    for job in jobs:
        n = job.name
	if n == "F0_P0_G0_M0_ST0" and job.nthread==1:
		ref = job.cputime
		print ref	 

	if len(n)<3: 
		
		fillWithPrefix("x: ", job,n[1],ref)
	else:
	        f = int(n[1])
        	p = int(n[4])
        	g = int(n[7])
        	m = int(n[10])

        	if f == 0 and p == 0 and g == 0  and m== 0 : fillWithPrefix(" ", job,n[:11],ref)
        	#if f == 0 and p == 0 and m == 0 : fillWithPrefix("G: ", job,g)
        	#if f == 0 and g == 0 and m == 0 : fillWithPrefix("P: ", job,p)
        	#if p == 0 and g == 0 and m == 0 : fillWithPrefix("F: ", job,f)
		#if f == 1 and p == 0 and g == 0 and m == 1 : refY = fillWithPrefix("x: ", job,f+m)        
		if f + p + g + m == 3 or f + p + g + m == 8 : fillWithPrefix("x ", job,n[:11],ref)

    beautify(hcpuPerEvent1T, 2, 1,3001)
    beautify(hcpuPerEvent4T, 4, 5,3001)
    beautify(hcpuPerEvent1AT, 1,9,3008)
    beautify(hcpuPerEvent4AT, 3,13,3008)

    ### setup canvas
    c1 = TCanvas('c1','c1', 900, 600 )
    #c1.Divide(1,5)
    c1.SetGrid()
#    frame1 = c1.DrawFrame(3600,0,4000,2)
#    frame1 = c1.DrawFrame(0,0.,8000,15)
#    frame1 = c1.DrawFrame(0,0.4,1000,1.31)
#    frame1.GetXaxis().SetTitle("Date")
#    frame1.GetYaxis().SetTitle("Value")

#    graphs[i].SetMarkerStyle(20+i)
#    graphs[i].SetMarkerColor(i+1)
#    graphs[i].SetLineColor(i+1)
#    graphs[i].Draw("")

    #.. plot 1
    #c1.cd(1)

    #print(0.002/ref,0.892/ref,0.818/ref,0.528/ref,0.218/ref)
    a = arr.array('d', [0,.002/ref,0,0,.818/ref,.528/ref,.218/ref])
    b = arr.array('d', [0,.892/ref,0,0,0,0])


    gStyle.SetErrorX(0.);
    gStyle.SetOptFit(1)
   # hcpuPerEvent1T.SetMarkerStyle(21);
    #hcpuPerEvent1T.SetMarkerColor(4);
    hcpuPerEvent1T.SetError(a)
    hcpuPerEvent1T.SetFillColorAlpha(20,0) 
   
    hcpuPerEvent1T.Draw('histbar')
    hcpuPerEvent1T.Draw('E1 same')
    #.. plot 2
    #c1.cd(2)
   # hcpuPerEvent4T.Draw('e histbar same')

    #c1.cd(3)
    hcpuPerEvent1AT.SetError(b) 
    hcpuPerEvent1AT.SetBarOffset(0.4)
    #hcpuPerEvent1AT.SetMarkerStyle(21);
    hcpuPerEvent1AT.SetFillColorAlpha(8,0) 
    hcpuPerEvent1AT.Draw('histbar same')
    
    hcpuPerEvent1AT.Draw('E same')

    #c1.cd(4)
    #hcpuPerEvent4AT.Draw('histbar same')


 
    legend = TLegend(0.1,0.7,0.48,0.9)
    legend.SetHeader("Process class","C") # option "C" allows to center the header
    legend.AddEntry(hcpuPerEvent1T,"1 thread","f")
    #legend.AddEntry(hcpuPerEvent4T,"4 thread","f")
    legend.AddEntry(hcpuPerEvent1AT,"1 thread and single track in 1","f")
    #legend.AddEntry(hcpuPerEvent4AT,"4 thread and single track in 1","f")
    legend.Draw()
    #c1.cd(5)
    c1.SaveAs("hcpuPerEvent.png")

    savefile.Write()
    savefile.Close()
