Characteristics:

geant4 10.04.p3 version


Ubuntu 16.04



gcc 5.4.0


veccore 0.5.2



Vc 1.3.3



R Core TM i7-4790HQ CPU @ 2.50GHz × 8 Intel


Instruction Set Extensions: AVX2



Cache: 8MB


* Level 1 cache size 4 x 32 KB 8-way set associative instruction caches
*  Level 1 cache size 4 x 32 KB 8-way set associative data caches 
* Level 2 cache size 4 x 256 KB 8-way set associative caches
* Level 3 cache size 8 MB 16-way set associative shared cache



![](hcpuPerEvent.png)


![](hCpuSpeedup.png)